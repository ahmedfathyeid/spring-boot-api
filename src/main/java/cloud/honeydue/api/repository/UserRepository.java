package cloud.honeydue.api.repository;

import cloud.honeydue.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> getByNickname(String nickname);
    public List<User> findAll();
}
